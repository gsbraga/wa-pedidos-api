import * as faker from 'faker/locale/pt_BR';
import * as Knex from 'knex';
import { ISolicitation } from 'modules/database/interfaces/solicitation';
import { IS_DEV } from 'settings';

export async function seed(knex: Knex): Promise<void> {
  if (!IS_DEV) return;

  const solicitations = await knex
    .count()
    .from('Solicitation')
    .first();

  const users = await knex
    .select('id')
    .from('User')
    .first();

  if (Number(solicitations.count) > 1) return;

  for (let x = 0; x < 10; x++) {
    const userId = users.id;
    const title = faker.name.jobTitle();
    const description = faker.name.jobDescriptor();
    const valueTotal = (x + 1) * 5;
    const quantity = (x + 1) * 2;

    const solicitaion: ISolicitation = {
      userId,
      title,
      description,
      valueTotal,
      quantity,
      createdDate: new Date(),
      updatedDate: new Date()
    };

    await knex.insert(solicitaion).into('Solicitation');
  }
}
