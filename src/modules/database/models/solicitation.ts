import { ApiProperty } from '@nestjs/swagger';
import { Model } from 'objection';

import { ISolicitation } from '../interfaces/solicitation';
import { User } from './user';

export class Solicitation extends Model implements ISolicitation {
  @ApiProperty({ type: 'number' })
  public id: number;
  @ApiProperty({ type: 'integer' })
  public userId?: number;
  @ApiProperty({ type: 'string' })
  public title: string;
  @ApiProperty({ type: 'string' })
  public description: string;
  @ApiProperty({ type: 'number' })
  public quantity: number;
  @ApiProperty({ type: 'number' })
  public valueTotal: number;
  
  @ApiProperty({ type: 'string', format: 'date-time' })
  public createdDate: Date;
  @ApiProperty({ type: 'string', format: 'date-time' })
  public updatedDate: Date;

  public user: User;

  public static get tableName(): string {
    return 'Solicitation';
  }

  public static get relationMappings(): any {
    return {
      user: {
        relation: Model.HasOneRelation,
        modelClass: User,
        filter: (query: any) => query.select('id', 'userId', 'title', 'description', 'quantity', 'valueTotal'),
        join: {
          from: 'User.id',
          to: 'Solicitation.userId'
        },
        where: {
          
        }
      }
    };
  }

  public $beforeInsert(): void {
    this.createdDate = this.updatedDate = new Date();
  }

  public $beforeUpdate(): void {
    this.updatedDate = new Date();
  }

  public $formatJson(data: ISolicitation): ISolicitation {
    return data;
  }

}
