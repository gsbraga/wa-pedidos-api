export interface ISolicitation {
  id?: number;
  userId?: number;
  title?: string;
  description?: string;
  quantity?: number;
  valueTotal?: number;

  createdDate?: Date;
  updatedDate?: Date;
}