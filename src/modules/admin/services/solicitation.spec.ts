import {ISolicitation } from 'modules/database/interfaces/solicitation';
import { ICurrentUser } from './../../common/interfaces/currentUser';
import { enRoles } from 'modules/database/interfaces/user';
import { SolicitationRepository } from '../repositories/solicitation';
import { SolicitationService } from './solicitation';

/* eslint-disable max-len */
describe('Admin/Solicitation', () => {
  let currentUser: ICurrentUser = {
    id : 1,
    firstName: 'firstName',
    lastName: 'lastName',
    email: 'test@email.com',
    roles: [enRoles.user]
  };
  let solicitationRepository: SolicitationRepository;
  let service: SolicitationService;
  const solicitation: ISolicitation = {
    userId: currentUser.id,
    title: 'Title',
    description: 'Descrição',
    quantity: 10,
    valueTotal: 100
  };

  beforeEach(async () => {
    solicitationRepository = new SolicitationRepository();

    service = new SolicitationService(solicitationRepository);
  });

  it('should create a Solicitation', async () => {
    
    jest.spyOn(solicitationRepository, 'insert').mockImplementationOnce(solicitation => Promise.resolve({ ...solicitation } as any));


    const result = await service.save(solicitation, currentUser);

    expect(result).not.toBeFalsy();
    expect(result).toEqual(solicitation);
  });

});
