import { Injectable } from '@nestjs/common';
import { ICurrentUser } from 'modules/common/interfaces/currentUser';
import { ISolicitation } from 'modules/database/interfaces/solicitation';
import { Solicitation } from 'modules/database/models/solicitation';

import { SolicitationRepository } from '../repositories/solicitation';

@Injectable()
export class SolicitationService {
  constructor(private solicitationRepository: SolicitationRepository) {}

  public async save(model: ISolicitation, currentUser: ICurrentUser): Promise<Solicitation> {

    if (model.id) return this.update(model);
    model.userId = currentUser.id;
    return this.create(model);
  }

  private async create(model: ISolicitation): Promise<Solicitation> {

    const solicitation = await this.solicitationRepository.insert(model);
    return solicitation;
  }

  public async update(model: ISolicitation): Promise<Solicitation> {

    const solicitation = await this.solicitationRepository.findById(model.id);
    return this.solicitationRepository.update({ ...solicitation, ...model });
  }
}
