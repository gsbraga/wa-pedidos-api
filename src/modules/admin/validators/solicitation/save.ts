import { ApiProperty } from '@nestjs/swagger';
import {
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  Min,
  MinLength
} from 'class-validator';
import { ISolicitation } from 'modules/database/interfaces/solicitation';

export class SaveValidator implements ISolicitation {
  @IsOptional()
  @IsInt()
  @Min(0)
  @ApiProperty({ required: false, type: 'integer' })
  public id?: number;

  @IsOptional()
  @IsString()
  @MaxLength(100)
  @ApiProperty({ required: false, type: 'string', maxLength: 100 })
  public title?: string;

  @IsNotEmpty()
  @MaxLength(500)
  @ApiProperty({ required: true, type: 'string', maxLength: 500 })
  public description: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  @MaxLength(50)
  @ApiProperty({ required: true, type: 'number', minLength: 1, maxLength: 10 })
  public quantity: number;

  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  @MaxLength(50)
  @ApiProperty({ required: true, type: 'number', minLength: 1, maxLength: 10 })
  public valueTotal: number;



}
