import { ICurrentUser } from './../../common/interfaces/currentUser';
import { Injectable } from '@nestjs/common';
import { IPaginationParams } from 'modules/common/interfaces/pagination';
import { ISolicitation } from 'modules/database/interfaces/solicitation';
import { Solicitation } from 'modules/database/models/solicitation';
import { Page, Transaction } from 'objection';

@Injectable()
export class SolicitationRepository {

  public async list(params: IPaginationParams, currentUser: ICurrentUser,  transaction?: Transaction): Promise<Page<Solicitation>> {
    let query = Solicitation.query(transaction)
      .select('*')
      .where('userId', '=', currentUser.id)
      .page(params.page, params.pageSize);

    if (params.orderBy) {
      if (params.orderBy !== 'description') {
        query = query.orderBy(params.orderBy, params.orderDirection);
      } else {
        query = query.orderBy('title', params.orderDirection);
      }
    }

    if (params.term) {
      query = query.where(query => {
        return query
          .where('title', 'ilike', `%${params.term}%`)
          .orWhere('description', 'ilike', `%${params.term}%`);
      });
    }

    return query;
  }

  public async findById(id: number, transaction: Transaction = null): Promise<Solicitation> {
    return Solicitation.query(transaction).findById(id);
  }

  public async insert(model: ISolicitation, transaction: Transaction = null): Promise<Solicitation> {
    return Solicitation.query(transaction).insert(model as any);
  }

  public async update(model: ISolicitation, transaction: Transaction = null): Promise<Solicitation> {
    return Solicitation.query(transaction).patchAndFetchById(model.id, model as any);
  }
}
