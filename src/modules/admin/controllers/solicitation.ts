import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Query, NotFoundException} from '@nestjs/common';

import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthRequired, CurrentUser } from 'modules/common/guards/token';
import { ICurrentUser } from 'modules/common/interfaces/currentUser';
import { Solicitation } from 'modules/database/models/solicitation';

import { SolicitationRepository } from '../repositories/solicitation';
import { SolicitationService } from '../services/solicitation';
import { UpdateValidator } from '../../app/validators/profile/update';
import { ListValidator } from '../validators/solicitation/list';
import { SaveValidator } from '../validators/solicitation/save';

@ApiTags('App: Solicitation')
@Controller('/solicitation')
@AuthRequired()
export class SolicitationController {
  constructor(private solicitationRepository: SolicitationRepository, private solicitationService: SolicitationService) {}

  @Get()
  @ApiResponse({ status: 200, type: Solicitation })
  public async getAll(@Query() model: ListValidator, @CurrentUser() currentUser: ICurrentUser) {
    const solicitations = this.solicitationRepository.list(model, currentUser);
    if (!solicitations) throw new NotFoundException();

    return solicitations;
  }

  @Get(':id')
  @ApiResponse({ status: 200, type: Solicitation })
  public async getById(@Param('id') id: number) {
    const solicitation = this.solicitationRepository.findById(id);
    if (!solicitation) throw new NotFoundException();

    return solicitation;
  }


  @Post()
  @ApiResponse({ status: 200, type: Solicitation })
  public async save(@Body() model: SaveValidator, @CurrentUser() currentUser: ICurrentUser) {
    return this.solicitationService.save(model, currentUser);
  }
}
